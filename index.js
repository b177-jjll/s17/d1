// JavaScript Array

// Array basic structure

/* Syntax
	let/const arrayName = [elementA, elementB, ...... , elementN];
*/

let grades = [98.5, 94.3, 89.2, 90.1];
	computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of mixed elements in an array but is not recommended

let	mixedArr = [12, 'Asus', null, undefined, true];

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass',
];

// Accessing elements in an array

console.log(grades);

console.log(computerBrands[2], computerBrands[7]);

console.log(computerBrands[4]);

// Reassign array values

console.log('Array before reassignment');
console.log(myTasks);

myTasks[0] = 'hello world';

console.log('Array after reassignment');
console.log(myTasks);

// Array Methods

// Mutator Methods - functions that mutate or change an array after they're created

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push() - adds an element in the end of an array and returns the array's length

/* Syntax
	arrayName.push(element);
*/

console.log('Current array');
console.log(fruits);

console.log(fruits.push('Mango'));
console.log('Mutated array from push method');
console.log(fruits);

// pop() - removes the last element in an array and returns the removed element

/* Syntax
	arrayName.pop();
*/

console.log(fruits.pop());
console.log('Mutated array from pop method');
console.log(fruits);

// unshift() - adds one or more elements at the beginning of an array

/* Syntax
	arrayName.unshift(elementA, .... , elementN);
*/

console.log(fruits.unshift('Lime', 'Banana'));
console.log('Mutated array from unshift method');
console.log(fruits);

// shift() - removes an element at the beginning of an array and returns the removed element

/* Syntax
	arrayName.shift();
*/

console.log(fruits.shift());
console.log('Mutated array from shift method');
console.log(fruits);

// splice() - simultaneously removes elements from a specified index number and add elements

/* Syntax
	arrayName.splice(startingIndex, deleteCount, elementToBeAdded);
*/

console.log(fruits.splice(1, 2, 'Lime', 'Cherry'));
console.log('Mutated array from splice method');
console.log(fruits);

// sort() - rearranges the array elements in alphanumeric order

/* Syntax
	arrayName.sort();
*/

console.log(fruits.sort());
console.log('Mutated array from sort method');
console.log(fruits);

// reverse() - reverses the order of array elements

/* Syntax
	arrayName.reverse();
*/

console.log(fruits.reverse());
console.log('Mutated array from reverse method');
console.log(fruits);

// Non-mutator Methods - functions that do not modify or change an array after they're created

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf() - returns an index number of the first matching element found in an array. If no match was found the result will be -1

/* Syntax
	arrayName.indexOf(searchValue);
*/

console.log('Result of indexOf method: '+ countries.indexOf('PH'));
console.log('Result of indexOf method: '+ countries.indexOf('BR'));

// lastIndexOf() - returns an index number of the last matching element found in an array.

/* Syntax
	arrayName.lastIndexOf(searchValue);
	arrayName.lastIndexOf(searchValue, fromIndex);
*/

// Getting the index number starting from the last element

console.log('Result of lastIndexOf method: '+ countries.lastIndexOf('PH'));

// Getting the index number starting from a specified index

console.log('Result of lastIndexOf method: '+ countries.lastIndexOf('PH', 6));

// slice() - portions/slices elements from array and returns the new array

/* Syntax
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, endingIndex);
*/

// Slicing off elements from a specified index to the last element

console.log('Result from slice method');
console.log(countries.slice(2));


// Slicing off elements from a specified index to another index

console.log('Result from slice method');
console.log(countries.slice(2, 4));

// toString() - returns an array as a string by commas

/* Syntax
	arrayName.toString();
*/

console.log('Result from toString method');
console.log(countries.toString());

// concat() - combines two arrays and returns the combined result

/* Syntax
	arrayA.concat(arrayB);
	arrayA.concat(elementA);
*/

let tasksA = ['drink html', 'eat javascript'];
	tasksB = ['inhale css', 'breathe sass'];
	tasksC = ['get git', 'be node'];

console.log('Result from concat method');
console.log(tasksA.concat(tasksB));

// Combining multiple arrays

console.log('Result from concat method');
console.log(tasksA.concat(tasksB, tasksC));

// Combining array with elements

console.log('Result from concat method');
console.log(tasksA.concat('smell express', 'throw react'));

// join() - returns an array as a string separated by specified separator string

/* Syntax
	arrayName.join('separatorString');
*/

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log('Result from join method');
console.log(users.join());

console.log('Result from join method');
console.log(users.join(' - '));

console.log('Result from join method');
console.log(users.join(''));

console.log('Result from join method');
console.log(users.join(', '));

// Iteration Methods - are loops designed to perform repetitive tasks on arrays

// forEach() - similar to a for loop that iterates on each array element

/* Syntax
	arrayName.forEach(function(indivElement) {
		statement/s;
	})
*/

let allTasks = tasksA.concat(tasksB, tasksC);

allTasks.forEach(function(task){
	console.log(task);
})

// Using forEach with conditional statements

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10) {
		filteredTasks.push(task);
	}
})

console.log('Result of filtered tasks:');
console.log(filteredTasks);

// map() - iterates on each element and returns a new array with different values depending on the result of the function's operation

/* Syntax
	let/const resultArray = arrayName.map(function(indivElement))
*/

let numbers = [1, 2, 3, 4, 5];
	numberMap = numbers.map(function(number) {
		return (number * number);
	})

console.log('Result of map method:');
console.log(numberMap);

// every() - checks if all elements in an array meet the given condition

/* Syntax
	let/const resultArray = arrayName.every(function(indivElement) {
		return condition;
	})
*/

let allValid = numbers.every(function(number) {
	return (number > 3);
})

console.log('Result of every method:');
console.log(allValid);

// some() - checks if at least one element in the array meets the given condition

/* Syntax
	let/const resultArray = arrayName.some(function(indivElement) {
		return condition;
	})
*/

let someValid = numbers.some(function(number) {
	return (number < 2);
})

console.log('Result of some method:');
console.log(someValid);

// filter() - returns a new array that contains elements which meets the given condition

/* Syntax
	let/const resultArray = arrayName.filter(function(indivElement) {
		return condition;
	})
*/

let filterValid = numbers.filter(function(number) {
	return (number < 3);
})

console.log('Result of filter method:');
console.log(filterValid);

// No elements found

let nothingFound = numbers.filter(function(number) {
	return (number === 0);
})

console.log('Result of filter method:');
console.log(nothingFound);

// Filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number) {
	if(number < 3) {
		filteredNumbers.push(number);
	}
})

console.log('Result of filter method:');
console.log(filteredNumbers);

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

// includes method

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes('a');
})

console.log('Result of includes method:');
console.log(filteredProducts);

// reduce() - evaluates elements from left to right and returns/reduces the array into a single value

/* Syntax
	let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
		return expression/operation;
	})
*/

let iteration = 0;

// numbers = [1, 2, 3, 4, 5]

let reducedArray = numbers.reduce(function(x, y) {
	console.warn('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	// Operation to reduce the array into a single value
	return x + y;
})

console.log('Result of reduce method')
console.log(reducedArray);

let list = ['Hello', 'Again', 'World'];
	reducedJoin = list.reduce(function(x, y) {
		return x + ' ' + y;
	})

console.log('Result of reduce method')
console.log(reducedJoin);

// Two-dimensional array - array within an array

/* Syntax
	let arrayName = [];
	let arrayName = [[], []];
*/

// 2 x 3 2-dimensional array

let twoDim = [[2, 4, 6], [8, 10, 12]];

console.log(twoDim[1][2]);
console.log(twoDim[0][1], twoDim[1][0]);

// Create a 3 x 2 2-dimensional array that contains random names
let names = [['John', 'Mary'], ['AC', 'DC'], ['Maria', 'Juan']];